#include <avr/sleep.h>

int FSR_Pin = A0; //analog pin 0
unsigned long echo = 0;
int ultraSoundSignal = 7; // Ultrasound signal pin
unsigned long ultrasoundValue = 0;
int incomingByte = 0;   // for incoming serial data
int byte2 = 0;

void setup(){
  
  Serial.begin(9600);
  
  //Proximity sensor:
  pinMode(ultraSoundSignal,OUTPUT);
  delay(5000);
  
}

unsigned long ping(){
   pinMode(ultraSoundSignal, OUTPUT); // Switch signalpin to output
   digitalWrite(ultraSoundSignal, LOW); // Send low pulse
   delayMicroseconds(2); // Wait for 2 microseconds
   digitalWrite(ultraSoundSignal, HIGH); // Send high pulse
   delayMicroseconds(5); // Wait for 5 microseconds
   digitalWrite(ultraSoundSignal, LOW); // Holdoff
   pinMode(ultraSoundSignal, INPUT); // Switch signalpin to input
   digitalWrite(ultraSoundSignal, HIGH); // Turn on pullup resistor
   echo = pulseIn(ultraSoundSignal, HIGH); //Listen for echo
   ultrasoundValue = (echo / 58.138) * .39; //convert to CM then to inches
   return ultrasoundValue;
}

void confirmWakeUp(){
  int notReady = 1;

  while (notReady){
    if (Serial.available()) {
      incomingByte = Serial.read();
      if (incomingByte == 'y'){
        notReady = 0;
      }
    }
  }
  loop();
}

void goToSleep(int number){
  
  int miliseconds = number * 1000;//only seconds right now, change this to 1000000 for minutes
  delay(miliseconds);
  confirmWakeUp();
  
}


void loop(){
  
  //pressure
  Serial.print("$pre");
  int FSRReading = analogRead(FSR_Pin); 

  Serial.println(FSRReading);
  delay(250);
  
  //motion
  Serial.print("$mot");
  Serial.println(digitalRead(2), DEC);
  delay(250);
  
  //proximity
  int x = 0;
  x = ping();
  Serial.print("$pro");
  Serial.println(x);
  delay(250); //delay 1/4 seconds.
  
  if (Serial.available()) {
    incomingByte = Serial.read();
    int number = incomingByte - 49;
    goToSleep(number);
  }
}
