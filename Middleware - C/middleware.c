#include "middleware.h"
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <arpa/inet.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

#define ARDUINO_DISCONNECTED 5 //seconds
#define DISABLE_AFTER 60 //seconds
#define TRUE 1
#define FALSE 0
#define MAX_ACCEPTABLE_MOUSE_DISTANCE 5
#define MIN_ACCEPTABLE_PRESSURE 100
#define MINUTE 25
#define NOTIFICATION_TIME 100 //seconds

//---------------------------------------------------------PROTOTYPES:

void check_if_past_threshold(); //"past_threshold?"
void check_time();  //will see if it's a new day and if it's time for an update
void handle_client_request(char*, char*);

int is_arduino_disconnected();
int is_at_computer(char*);
int is_daily_usage_request(char*);
int is_productivity_update(char*);
int is_session_len_request(char*);
int is_stream_request(char*);
int is_time_to_notify_request(char*);
int is_timeout(char*);
int is_toggle(char*);
int is_wake_up_call(char*); 
void make_arduino_sleep(int); //makes arduino sleep for x minutes
void *read_handler(void *);
void sig_handler(int sig_num);
void send_daily_usage_stat(char*); //TODO
void send_if_at_computer(char*);
void send_if_notification_time(char*);
void send_session_length(char*);
void send_productivity_updated_confirmation(char*); //receive a string "productivity:PXXUXX" //P for productive, U for unproductive
void set_session_threshold(int min); //sets a new max lession length
void send_hourly_update(); //sends to client 
void send_streaming_scores(char*);  //rates the current pressure of user and sends to java
void send_wake_up_call(char*);
void setup(int);
int set_up_server(int, int*);
void start_new_day(); //increments todays_index, clears necessary fields...
void start_server(int);
int substring(char*, char*, int, int);
void toggle_app_running(char*);
void update_and_confirm(char*, char*);
void update_date(); //checks time and updates todays_index if necessary
void update_readings(char*);
void update_slouch_count();
void update_statistics();
void update_time_stuff();

//-------------------------------------------------------------FIELDS:

int active; //Boolean - True if user is at computer
int arduino_fd;
int android_fd;
int app_running; //Boolean - True if android application is running
int can_send_offline_notification;
int daily_num_reads[7]; //total number of updates each day
int daily_productive_minutes[7];
int daily_slouch_readings[7]; //number of bad posture readings each day
int daily_usage[7]; //seconds spent on computer each day
int days_monitored; //total days this program has been running
time_t last_arduino_contact;
time_t last_stat_update_time;
pthread_mutex_t lock; //for update statistics
int max_session_length; //longest tolerable session length (in minutes)
int motion_reads; //necessary?
int num_updates;
int pressure_reads;
int proximity_reads;
int pressure_readings[10000];
time_t program_start_time;
int proximity_readings[10000];
time_t session_start_time; //in seconds (since 1970)
time_t session_length; //in seconds
int sock; //socket used int set_up_server()
int streaming_mode;
struct termios options;
int timing;
int todays_index; //index into daily_slouching and daily_usage
int unacceptable_slouchage; //highest ratio of slouch minutes to usage minutes

//-----------------------------------------------------------MAIN:

int main(int argc, char * argv[]){

  pthread_t thread;
  int port_num;

  timing = TRUE;
  app_running = FALSE;
  can_send_offline_notification = FALSE;
  streaming_mode = FALSE;

  active = FALSE;
  days_monitored = 0;
  last_stat_update_time = 0;
  num_updates = 0;
  pressure_reads = 0;
  proximity_reads = 0;
  bzero(daily_usage, 7);
  bzero(daily_slouch_readings, 7);
  bzero(daily_productive_minutes, 7);
  session_length = 0;
  program_start_time = time(NULL);
  //get the port number from the command line argument
  if (argc == 2)   port_num = atoi(argv[1]);
  else {
    printf("Need a port number to run program!  Try again: ./middleware [PORT_NUM]\n");
  }
  //set up the lock
  pthread_mutex_init(&lock, NULL);
  //create a thread to read and record measurements from the arduino
  pthread_create(&thread, NULL, read_handler, NULL);

  start_server(port_num);
 
  return 0;   
}

//-----------------------------------------------------------METHODS:

/*
 * Calculates the number of minutes spent slouching on days[index]
 */
int get_minutes_slouching(int index) {

  double slouch_percentage = 1.0 * daily_slouch_readings[index] / 
                            daily_num_reads[index];

  //daily_usage is in seconds, need minutes
  return (int)(slouch_percentage * (daily_usage[index] / MINUTE));

}

/*
 * Parses strings sent by client (in_buffer), calls the appropriate 
 * method and populates the out_buffer with requested information
 */
void handle_client_request(char* in_buffer, char* out_buffer) {

  char to_arduino[1];
  printf("enter handle client request\n");
  pthread_mutex_lock(&lock);
  if (is_arduino_disconnected()) {
    printf("arduino disconnected\n");
    exit(0);
  }
  pthread_mutex_unlock(&lock);

  if (is_at_computer(in_buffer)) { 
    printf("is time to notify\n");
    send_if_at_computer(out_buffer);
  }
  else if (is_toggle(in_buffer)) {
    printf("toggling app_running\n");
    toggle_app_running(out_buffer);
  }
  else if (is_productivity_update(in_buffer)) {
    printf("updating productivity statistics\n");
    update_and_confirm(in_buffer, out_buffer);
  }
  else if (is_wake_up_call(in_buffer)) {
    printf("received wake-up call\n");
    pthread_mutex_lock(&lock); //uses arduino_fd
    send_wake_up_call(out_buffer);
    pthread_mutex_unlock(&lock);
  }
  else if (is_stream_request(in_buffer)) {
    printf("stream_request\n");
    streaming_mode = TRUE;
    pthread_mutex_lock(&lock);
    send_streaming_scores(out_buffer);
    pthread_mutex_unlock(&lock);
  }
  else if (is_session_len_request(in_buffer)) {
    printf("session_length_request\n");
    pthread_mutex_lock(&lock);
    send_session_length(out_buffer);
    pthread_mutex_unlock(&lock);
  }
  else if (strncmp(in_buffer, "end_stream", 10) == 0) {
    printf("not sending anymore streaming info\n");
    strcpy(out_buffer, "stream_ended\n");
    streaming_mode = FALSE;
  }
  else if (is_daily_usage_request(in_buffer)) {
    printf("session_daily_usage_request!\n");
    pthread_mutex_lock(&lock);
    send_daily_usage_stat(out_buffer);
    pthread_mutex_unlock(&lock);
  }
  else if (is_timeout(in_buffer)){ //we have received a timeout request from android
    printf("timeout_request\n");
    to_arduino[0] = in_buffer[7];
    timing = FALSE;
    pthread_mutex_lock(&lock);
    //sends a message to arduino to make it sleep
    if (write(arduino_fd, to_arduino, sizeof(char)) == -1)   printf("problem writing in timeout\n");
    pthread_mutex_unlock(&lock);
    strcpy(out_buffer, "timeout_request_received\n");
  }
  else {
    printf("Got a request I didn't understand:\n%s\n", in_buffer);
  }

}

/*
 * Returns true if we haven't heard from arduino in a while
 */
int is_arduino_disconnected() {

  if (timing) {
    if (time(NULL) - last_arduino_contact > ARDUINO_DISCONNECTED) {
      return TRUE;
    }
  }
  return FALSE;

}

/*
 * Returns TRUE if user is at the computer
 */
int is_at_computer(char* in_buffer) {

  return (strncmp(in_buffer, "at_computer", 11) == 0);

}

/*
 * Returns TRUE if buffer holds a request for usage statistics
 */
int is_daily_usage_request(char* buffer) {

  return (strncmp(buffer, "send_usage", 10) == 0);

}

/*
 * Returns TRUE if buffer holds a request for the duration of current session
 */
int is_session_len_request(char* buffer) {

   return (strncmp(buffer, "send_session_len", 16) == 0);
}

/*
 * Returns TRUE if android wants streaming information
 */
int is_stream_request(char* buffer) {

  //length of buffer should be 7
  if (strlen(buffer) != 7){
    return FALSE;
  }
  if (strncmp(buffer, "stream", 6) == 0){
    return TRUE;
  }
  return FALSE;//added this to always return a value
}

/*
 * Returns TRUE if client is telling us whether or not user has been 
 * productive since last update
 */
int is_productivity_update(char* in_buffer) {

  return (strncmp("productivity_", in_buffer, 13) == 0);

}

/*
 * Returns TRUE if the user wants to put the arduino to sleep
 */
int is_timeout(char * msg){

  char timeout[8];
  int i;
  for(i = 0; i < 7; i++){
    timeout[i] = msg[i];
  }
  timeout[7] = '\0';
  if(strcmp(timeout, "timeout") == 0){

    if (msg[7] == '5' || msg[7] == '7'  || msg[7] == '9' ){ 
      return 1;
    }
  }
  return 0;

}

/*
 * Returns TRUE if the client is closing/opening the application
 */
int is_toggle(char* in_buffer) {

  return (strncmp("running_toggle", in_buffer, 14) == 0);

}

/*
 * Returns TRUE if the client wants to wake up the arduino
 */
int is_wake_up_call(char* in_buffer) {

  return strncmp(in_buffer, "wake_up", 7) == 0;

}

/*
 * Returns the smaller of the two parameters
 */
int min(int a, int b) {

  if (a < b)   return a;

  return b;

}

/*
 * Loop for interacting with Arduino.  First opens a socket for 
 * communication and then constantly processes the latest readings
 */
void * read_handler(void * dummy){ 

  char buffer[100];
  char just_read[10];
  int bytes_read = 0;
  int total_read = 0;

  arduino_fd = open("/dev/ttyUSB10", O_RDWR);

  if (arduino_fd == -1)    printf("Error reading from USB10\n");
    
  setup(arduino_fd);
  
  bzero(buffer, 100);
  bzero(just_read, 10);

  while(TRUE) {
    
    bytes_read = read(arduino_fd, just_read, 10);
    strncat(buffer, just_read, (size_t)bytes_read);
    total_read += bytes_read;

    //check_if_active()   //will compare current time to time of last event, set active to 0 if it's been 4 minutes

    if (buffer[total_read - 1] == '\n') { //we have received a complete message from arduino
      
      printf("RECEIVED: %s", buffer);
      
      pthread_mutex_lock(&lock);
      update_readings(buffer);
      pthread_mutex_unlock(&lock);
 
      buffer[total_read - 1] = '\0';
      bzero(buffer, 99);
      total_read = 0;
    }
        
  }
}

/*
 * Closes connections to android and arduino on SIGKILL
 */
void sig_handler(int sig_num) {

  if (sig_num == SIGKILL) {
    if (close(android_fd) == -1)   printf("problem closing android!\n");
    if (close(sock) == -1)   printf("problem closing android!\n");
    exit(0);

  }

}

/*
 * Populates out_buffer with information on minutes at computer, minutes
 * being productive, and minutes slouching in the last seven days
 *
 * Format:
 * [minutes used each day].[minutes slouch].[minutes productive]
 * "#XXX.XXX.XXX#XXX.XXX.XXX" 
 */
void send_daily_usage_stat(char* out_buffer) {

  int i, days_to_read, used, slouched, productive;
  int index;// = (days_monitored - i) % 7;
  char one_day[50];

  if (strlen(out_buffer) != 0)   printf("gotta bzero this \n");

  if (days_monitored > 6)   days_to_read = 6;
  else   days_to_read = days_monitored;

  for (i = 0; i <= days_to_read; i++) {

    index = (days_monitored - i) % 7;
    //minutes on computer
    used = daily_usage[index] / MINUTE;
    //minutes slouched
    slouched = get_minutes_slouching(index);
    //minutes productive
    productive = daily_productive_minutes[index];
    //get the info for the current day
    sprintf(one_day, "#%d.%d.%d", used, productive, slouched);
    //add it to the end of the out buffer
    strcat(out_buffer, one_day);

  }

  //Client needs a newline to receive (using buffered reader)
  strcat(out_buffer, "\n");

  printf("out_buffer looks like this: %s\n", out_buffer);

  //final out_buffer should be "#XXX.XXX.XXX#XXX.XXX.XXX...#XXX.XXX.XXX"

} 

/*
 * Populates out_buffer with "yes" or "no", depending on whether or
 * not user is at computer
 */
void send_if_at_computer(char* out_buffer) {

  if (can_send_offline_notification == TRUE) {
    strcpy(out_buffer, "yes\n");
    can_send_offline_notification = FALSE;
  }

  else strcpy(out_buffer, "no\n");

}


/*
 * Updates field daily_productive_minutes based on the client's message
 * and then populates out_buffer with confirmation
 */
void update_and_confirm(char* in_buffer, char* out_buffer) {

  if (in_buffer[13] == 'y') { //TODO explain...
    printf("updating productive minutes by %d\n", (NOTIFICATION_TIME / MINUTE));

    daily_productive_minutes[todays_index] += (NOTIFICATION_TIME / MINUTE);
  }


  strcpy(out_buffer, "updated productivity!\n");

}

/*
 * Populates out_buffer with length (in minutes) of current session
 */
void send_session_length(char* out_buffer) {

  //session_length is in seconds, and we want minutes (it will round down)
  int session_len =(int)(1.0 * session_length / MINUTE);

  sprintf(out_buffer, "len:%d\n", (int)session_len);//maybe %02d

}

/*
 * Sends the slouch ratios of the all readings currently in read buffers
 */
void send_streaming_scores(char* out_buffer) {

  int num_good_proximity_reads;
  int num_good_pressure_reads;
  int i;
  float pre_ratio, pro_ratio;

  num_good_proximity_reads = 0;
  num_good_pressure_reads = 0;

  if (pressure_reads == 0 || proximity_reads == 0) {
      strcpy(out_buffer, "$pre0.0$pro0.0\n");
      printf("exit send_streaming without readings\n");
      return;
    }

  for (i = 0; i < pressure_reads; i++) {

    if (pressure_readings[i] >= MIN_ACCEPTABLE_PRESSURE) {

        num_good_pressure_reads++;
      }
  }
  for (i = 0; i < proximity_reads; i++) {

    if (proximity_readings[i] <= MAX_ACCEPTABLE_MOUSE_DISTANCE) {

        num_good_proximity_reads++;
    }
  }
  pre_ratio = (1.0 * num_good_pressure_reads) / pressure_reads;
  pro_ratio = (1.0 * num_good_proximity_reads) / proximity_reads;

  update_statistics();

  sprintf(out_buffer, "$pre%.1f$pro%.1f\n", pre_ratio, pro_ratio);
  printf("exit send_streaming\n");
}

/*
 * Sends arduino a wake up message and populates out_buffer with a
 * confirmation message
 */
void send_wake_up_call(char* out_buffer) {

  char arduino_buffer[2];
  last_arduino_contact = time(NULL);
  timing = TRUE;
  arduino_buffer[0] = 'y';
  // arduino_buffer[1] = '\0';
  
  if (write(arduino_fd, arduino_buffer, sizeof(char)) == -1)  printf("problem with write!\n");

  strcpy(out_buffer, "woke_up_arduino\n");

}

/*
 * Sets up struct terminos for communication with arduino
 */
void setup(int fd) {

  if (tcgetattr(fd, &options) == -1)   printf("problem in arduino setup\n");
  if (cfsetispeed(&options, 9600) != 0)   printf("problem in arduino setup\n");
  if (cfsetospeed(&options, 9600) != 0)   printf("problem in arduino setup\n");
  if (tcsetattr(fd, TCSANOW, &options) == -1)   printf("problem in arduino setup\n");
 
}

/*
 * Sets up a socket connection for communication with Android 
 */ 
int set_up_socket(int PORT_NUMBER, int* sock) {

  // structs to represent the server and client
  struct sockaddr_in server_addr, client_addr;    
  int temp, fd;
  size_t sin_size;
      
  // 1. socket: creates a socket descriptor that you later use to make other system calls
  if ((*sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("Socket");
    exit(EXIT_FAILURE);
  }
  if (setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, &temp, (socklen_t)sizeof(int)) == -1) {
    perror("Setsockopt");
    exit(EXIT_FAILURE);
  }

  // configure the server
  server_addr.sin_port = htons(PORT_NUMBER); // specify port number
  server_addr.sin_family = AF_INET;         
  server_addr.sin_addr.s_addr = INADDR_ANY; 
  bzero(&(server_addr.sin_zero),8); 
      
  // 2. bind: use the socket and associate it with the port number
  if (bind(*sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1) {
    perror("Unable to bind");
    exit(0);
  }

  // 3. listen: indicates that we want to listn to the port to which we bound; second arg is number of allowed connections
  if (listen(*sock, 5) == -1) {
    perror("Listen");
    exit(0);
  }
          
  printf("\nServer waiting for connection on port %d\n", PORT_NUMBER);
  if (fflush(stdout) != 0)   printf("fflush ffailed\n");

  //---------------------------------------------------------------------------     

  // 4. accept: wait until we get a connection on that port
  sin_size = sizeof(struct sockaddr_in);
  fd = accept(*sock, (struct sockaddr *)&client_addr,(socklen_t *)&sin_size);

  return fd;
}

/*
 * Starts a new session and resets appropriate fields
 */
void start_new_session() {

  printf("---------------------------------STARTING NEW SESSION!\n");
  can_send_offline_notification = TRUE;
  session_start_time = time(NULL);
  //mark the time of this update
  last_stat_update_time = time(NULL);

}

/*
 * Starts the server and awaits for a connection from an android.  
 * Upon connection, it will loop and handle all requests until 
 * connection is closed.
 */
void start_server(int PORT_NUMBER){

  // buffer to read data into
  char recv_data[1024];
  char send_data[1000];

  android_fd = set_up_socket(PORT_NUMBER, &sock);
  if (signal(SIGKILL, sig_handler) == SIG_ERR)   printf("error receiving signal\n");

  while(TRUE){    
    // 5. recv: read incoming message into buffer
    int bytes_received = (int)recv(android_fd,recv_data,1024,0);
    // null-terminate the string
    recv_data[bytes_received] = '\0';

    printf("Server received message: %s\n", recv_data);

    if (strcmp(recv_data, "quit\n") == 0)   break;

    else   handle_client_request(recv_data, send_data);

    // strcpy(send_data, "Hi java!\n");

    if (strlen(send_data) != 0){

      if (send(android_fd, send_data, strlen(send_data), 0) == -1)   printf("Problem sending\n");
      printf("Server sent message: %s\n", send_data);

    }
    bzero(recv_data, 1024);
    bzero(send_data, 1000);
  }

} 

/*
 * Populates dest with a new string that is a substring of source
 *
 * From begin_index to end_index (exclusive) of source
 */
int substring(char* source, char* dest, int begin_index, int end_index) {

  int i, j;

  // int dest_len = end_index - begin_index + 1;
  if (source == NULL || dest == NULL)   return -1;
  if (strlen(source) + 1 < end_index)   return -1;
 
  j = 0; 

  for (i = begin_index; i < end_index; i++) {
    
    dest[j] = source[i];
    j++;

  }

  dest[j] = '\0';

  return TRUE; //on success

}

/*
 * Sets can_send_offline_notification to FALSE and populates out_buffer
 * with confirmation message
 */
void toggle_app_running(char* out_buffer) {

  can_send_offline_notification = FALSE;

  strcpy(out_buffer, "gotcha\n");

}

/*
 * This is called whenever arduino reads something.  This method stores
 * what it reads in the appropriate temprory buffer until long-term
 * usage statistics are updated
 * 
 */
void update_readings(char* buffer) {

  char number[10];

  //update session length (may end session) and todays_index (if necessary)
  update_time_stuff();

  //check if the input is formatted correctly
  if (buffer[0] != '$')  {
    pthread_mutex_unlock(&lock);
    printf("Bad read! Oh no!\n");
    return;
  }

  if (buffer[3] == 't') { //---------------motion reading

    if (buffer[4] == '1') { //motion was sensed

      if (active == TRUE) {

        if (streaming_mode == FALSE) { //streaming mode will call update statistics if active
	  
          update_statistics();
        }

      }
      else   { //first motion read since last session ended
	pthread_mutex_unlock(&lock);
        start_new_session();
      }
      //we are now/still active
      active = TRUE;

    }

  }
  if (buffer[3] == 'o')  { //-----------proximity reading

    substring(buffer, number, 4, strlen(buffer) + 1);
    proximity_readings[proximity_reads] = atoi(number);
    proximity_reads++;

  }
  if (buffer[3] == 'e') { //-------------pressure reading
    
    if (substring(buffer, number, 4, strlen(buffer) + 1) == -1) {
      printf("There has been a substring error!\n");
    }
    pressure_readings[pressure_reads] = atoi(number);
    pressure_reads++;

  }

}

/*
 * 
 */
void update_slouch_count() {

  int i;
  int complete_readings = min(pressure_reads, proximity_reads);
  
  //iterate over all recent pressure and proximity reads...
  for (i = 0; i < complete_readings; i++) {
    //count the times we had imperfect posture
    if (pressure_readings[i] < MIN_ACCEPTABLE_PRESSURE ||
	proximity_readings[i] > MAX_ACCEPTABLE_MOUSE_DISTANCE) {

      daily_slouch_readings[todays_index]++;
    } 
  }

}


/*
 * Updates statistics or sets 
 */
void update_statistics() {

  time_t now = time(NULL);

  //update the number of complete readings
  daily_num_reads[todays_index] += min(proximity_reads, pressure_reads);
  //count the number of poor pressure and proximity reads
  update_slouch_count();
  
  daily_usage[todays_index] += now - last_stat_update_time;

  proximity_reads = 0;
  pressure_reads = 0;
  last_stat_update_time = now;

}

/*
 * updates the current_session_time and the day of the week
 */
void update_time_stuff() {

  //number of seconds since Jan 1, 1970
  time_t right_now = time(NULL);
  //this method is only called when readings are updated
  last_arduino_contact = right_now;
  //check if program is still active
  if (active == TRUE && 
      (right_now - last_stat_update_time) >= DISABLE_AFTER && 
      timing == TRUE) {

    session_length = 0;
    active = FALSE;

  }
  
  if (active == TRUE) {
    
    session_length = right_now - session_start_time;
  }
  //86400 seconds in a day
  days_monitored = (int)((right_now - program_start_time) / 86400);
 
  //seven days in a week
  todays_index = (days_monitored) % 7;

}


