#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <arpa/inet.h>
#include <termios.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>

//---------------------------------------------------------PROTOTYPES:

void check_if_past_threshold();
void check_time();
void handle_client_request(char*, char*);

int is_arduino_disconnected();
int is_at_computer(char*);
int is_daily_usage_request(char*);
int is_productivity_update(char*);
int is_session_len_request(char*);
int is_stream_request(char*);
int is_time_to_notify_request(char*);
int is_timeout(char*);
int is_toggle(char*);
int is_wake_up_call(char*); 
void make_arduino_sleep(int);
void * read_handler(void *);
void sig_handler(int sig_num);
void send_daily_usage_stat(char*);
void send_if_at_computer(char*);
void send_if_notification_time(char*);
void send_session_length(char*);
void send_productivity_updated_confirmation(char*);
void set_session_threshold(int min);
void send_hourly_update(); 
void send_streaming_scores(char*);
void send_wake_up_call(char*);
void setup(int);
int set_up_server(int, int*);
void start_new_day(); 
void start_server(int);
int substring(char*, char*, int, int);
void toggle_app_running(char*);
void update_and_confirm(char*, char*);
void update_date(); 
void update_readings(char*);
void update_slouch_count();
void update_statistics();
void update_time_stuff();
