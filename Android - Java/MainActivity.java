package com.example.trackme;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Stack;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * MainActivity Class
 *  ~ Home Screen, launches all other Activities
 * @authors Alex Miller, Connor Liddic
 */
public class MainActivity extends SuperActivity {
    
    static Stack<Activity> activityStack = new Stack<Activity>();
    static Activity currentActivity;
    static boolean appIsRunning;
    static boolean firstTimeRunning = true;
    static String time = "0";
    static int[] usageMinutes = {100, 200, 150, 250}; 
    static int[] productiveMinutes = {80, 180, 104, 150};
    static int[] slouchMinutes = {40, 100, 63, 14};
    static Button notificationButton;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        
        //set control booleans
        appIsRunning = true;
        SecretService.serviceIsActive = true;
        boolean noErrors = true;
        
        //call constructors, set up view, register receiver
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerReceiver(batInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        
        //if no connection to server, set it up
        if (firstTimeRunning) {

            Client client = Client.getClient();
            try {
                Client.setUpConnection(12345);
            } catch (UnknownHostException e) {
                launchQuitPrompt();
                noErrors = false;
            } catch (IOException e) {
                launchQuitPrompt();
                noErrors = false;
            }
            firstTimeRunning = false;
        }
        
        //if connection set up cleanly, launch AsynchTasks
        if (noErrors) {

            //launch service
            Intent i = new Intent(this, SecretService.class);
            startService(i);
            
            //set up and launch all AsynchTasks
            TextView textView = (TextView)findViewById(R.id.totalTime);
            new TrackTask().execute("running_toggle");
            new StatusTask().execute();
            new NotificationTask().execute();
            UpdateTask t = new UpdateTask();
            t.setView(textView, this);
            t.execute("send_session_len");  
            
            //push this activity onto the Stack
            activityStack.push((Activity)this);
            currentActivity = this;
            
        }
    }

    //-------------------------------------------------------onClick() Handlers
    
    /**
     * Listener for stat button, launches the StatActivity
     * @param view - Called from
     */
    public void onStatClick(View view) {
        Intent intent = new Intent(this, StatActivity.class);
        startActivityForResult(intent, 1);
    }
    
    /**
     * Listener for stream button, launches the StreamActivity
     * @param view - Called from
     */
    public void onStreamClick(View view) {
        Intent intent = new Intent(this, StreamActivity.class);
        startActivityForResult(intent, 1);
    }
    
    /**
     * Listener for timeout button, launches the TimeoutActivity
     * @param view - Called from
     */
    public void onTimeoutClick(View view) {
        launchTimeoutScreen();
    }
    
    /**
     *  Listener for the Quit Button, kills the receiver,
     *  sends a message to server saying it's turning off
     * @param view - Call from
     */
    public void onQuitClick(View view) {
        appIsRunning = false;
        new TrackTask().execute("running_toggle");
        activityStack.clear();
        killReceiver();
        finish(); 
    }
    
    /**
     * Listener for Notification Button, launches the NotificationActivity
     * @param view - launched from
     */
    public void onSetNotificationClick(View view) {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivityForResult(intent, 1);
    }
    
    //----------------------------------------------------------------------Battery Receiver Methods
    
    /**
     * Wrapper method to unregister the battery receiver
     */
    public void killReceiver() {
        unregisterReceiver(batInfoReceiver);
    }
    
    /**
     *  Static inner class for the Broadcast Receiver
     *  This listens for changes in the battery level and changes the rate at which
     *  the Android sends and receives data
     */
    private static BroadcastReceiver batInfoReceiver = new BroadcastReceiver() {
        
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra("level", 0);
            
            //print to log to show that the battery level changed
            Log.i("MainActivity", "Power level changed! Updating sleep length accordingly");
            
            if (level < 10) setSleepLength(30000); //sleep for 30 seconds between each update
            else if (level < 40)   setSleepLength(10000); //sleep for 10 seconds between each update
            else   setSleepLength(5000); //sleep for five seconds between updates
            
        }
        
    };
    
    
    //---------------------------------------------------------------------------HELPERS
    
    /**
     * Used to change the frequency that the Android program communicates with the C server
     * @param miliseconds - Miliseconds that you want to set the sleepLength to.
     */
    public static void setSleepLength(int miliseconds) {
        StreamTask.sleepLength = miliseconds;
        UpdateTask.sleepLength = miliseconds;
        SecretService.sleepLength = miliseconds;
        NotificationTask.sleepLength = miliseconds;
    }
    
    /**
     * Listener for Quit button
     */
    public void launchQuitPrompt() {
        this.removeDialog(2);
        this.showDialog(2);
    }
    
    /**
     * Wrapper function to launch the Timeout Screen
     */
    public void launchTimeoutScreen() {
        Intent intent = new Intent(this, TimeoutActivity.class);
        startActivityForResult(intent, 1);
    }
    
}
