package com.example.trackme;

import java.util.ArrayList;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;

/**
 * Graph Class
 *  ~ Creates the Graph
 * @authors Alex Miller, Connor Liddic
 */
public class GraphView extends View {
    
    final static int LEGEND_SIZE = 26; //font size
    final static int BUFFER = 5;
    final static int[] days = { Days.SUNDAY, Days.MONDAY, Days.TUESDAY, Days.WEDNESDAY, Days.THURSDAY, Days.FRIDAY, Days.SATURDAY};
    static int minuteHeight = 1;
//------------------------------------------------------------FIELDS:
    
    
    static GraphView currentGraphView;
    
    static int viewWidth;
    static int viewHeight;
    static boolean showStatistics = false;
    static boolean showGraph = false;
    
    int dayWidth; //the width of each day (three bars for usage, productivity, and slouchosity)
    int barWidth; //the width of each individual bar
    
    int today;
    int numDays; //should be set to the length of each of the following:
    int[] usageMinutes;
    int[] productiveMinutes; 
    int[] slouchMinutes;
    
    int[] colors;
    
    ArrayList<ShapeDrawable> usageRects; 
    ArrayList<ShapeDrawable> productiveRects; 
    ArrayList<ShapeDrawable> slouchRects; 
    
    ShapeDrawable square = new ShapeDrawable(new RectShape());
        
//------------------------------------------------------CONSTRUCTORS:
        
    public GraphView(Context context) {
        
        super(context);
        
    }
    
    public GraphView(Context context, AttributeSet attributeSet) {
        
        super(context);
        currentGraphView = this;

    }
    
//-----------------------------------------------------------METHODS:
    
    /**
     * Creates the actual rectangles that form the graph
     * @param shapes
     * @param times
     * @param statIndex
     */
    private void createRects(ArrayList<ShapeDrawable> shapes, int[] times, int statIndex) {
        
        
        int left, top, right, bottom = viewHeight;
        
        
        for (int i = 0; i < numDays; i++) {
            
            //calculate boundries (bottom always the same)
            left = i * (dayWidth + BUFFER) + statIndex * barWidth; 
            top = viewHeight - times[i] * minuteHeight;
            right = left + barWidth;
            
            //create new rectangle
            ShapeDrawable rect = new ShapeDrawable(new RectShape());
            rect.setBounds(left, top, right, bottom);
            
            //select a color based on which graph we're drawing
            rect.getPaint().setColor(colors[statIndex]);
            shapes.add(rect);
            
        }
        
    }
    
    /**
     * Draws the arrays
     * @param canvas - that will be drawn on
     */
    private void drawArrays(Canvas canvas) {
        
        for (int i = 0; i < usageRects.size(); i++) {
            usageRects.get(i).draw(canvas);
        }
        for (int i = 0; i < productiveRects.size(); i++) {
            productiveRects.get(i).draw(canvas);
        }
        for (int i = 0; i < slouchRects.size(); i++) {
            slouchRects.get(i).draw(canvas);
        }
        
    }
    
    /**
     * Draws days
     * @param canvas - that will be drawn on
     */
    private void drawDays(Canvas canvas) {
        
        for (int i = 0; i < numDays; i++) {
            //usageMinutes will always be greater than the other statistics, so we write the day on top of that (highest bar)
            int x = usageRects.get(i).getBounds().left;
            int y = usageRects.get(i).getBounds().top - BUFFER;
            
            if (i == 0)   drawDay(canvas, x, y, "Today");
            else if (i == 1)   drawDay(canvas, x, y, "Yesterday");          
            else drawDay(canvas, x, y, dayToString(days[(today - i) % 7])); //+1 because enums start at
            
        }
        
    }
    
    /**
     * Determines how many pixels a minute should be
     */
    public void setMinuteHeight() {
        
        int longestTime = 0;
        
        for (int i = 0; i < numDays; i++) {
            
            if (usageMinutes[i] > longestTime)   longestTime = usageMinutes[i];
            if (productiveMinutes[i] > longestTime)   longestTime = usageMinutes[i];
            if (slouchMinutes[i] > longestTime)   longestTime = usageMinutes[i];
            
        }
        
        //we want the longest time to go about 70% up the view
        minuteHeight = (int)((viewHeight - (viewHeight * 0.6)) / longestTime);
        
        if (minuteHeight > 10)   minuteHeight = 4;
        
    }

    /**
     * Helper to convert the int day to a String
     * @param day - to get the string of
     * @return String of the day requested
     */
    String dayToString(int day) {
        
        switch(day) {
            case Days.SUNDAY:
                return "Sunday";
            case Days.MONDAY:
                return "Monday";
            case Days.TUESDAY:
                return "Tuesday";
            case Days.WEDNESDAY:
                return "Wednesday";
            case Days.THURSDAY:
                return "Thursday";
            case Days.FRIDAY:
                return "Friday";
            case Days.SATURDAY:
                return "Saturday";
            default:
                return "Not a day!";
        }
    }
    
    /**
     * Draws that day's graph
     * @param canvas - to draw on
     * @param x - x coordinate
     * @param y - y coordinate
     * @param day - to write
     */
    void drawDay(Canvas canvas, int x, int y, String day) {
        
        Paint p = new Paint();
        p.setColor(Color.BLACK);
        p.setTextAlign(Paint.Align.LEFT);
        p.setTypeface(Typeface.SANS_SERIF);
        p.setTextSize(1 + 80 / numDays); //TODO scale to numDays
        canvas.drawText(day, x, y, p);
        
        
    }
    
    /**
     * Draws the legend
     * @param canvas - to draw on
     */
    void drawLegend(Canvas canvas) {
        
        Paint p = new Paint();
        p.setColor(Color.BLACK);
        p.setTextAlign(Paint.Align.LEFT);
        p.setTypeface(Typeface.SANS_SERIF);
        p.setTextSize(LEGEND_SIZE);
        
        canvas.drawText("Blue = Total Minutes at Computer", 0, LEGEND_SIZE, p);
        canvas.drawText("Red = Productive Minutes", 0, 2 * (BUFFER + LEGEND_SIZE), p);
        canvas.drawText("Green = Slouching Minutes", 0, 3 * (BUFFER + LEGEND_SIZE), p);
        
    }

    /**
     * Draws the chart
     * @param canvas - to draw one
     */
    void drawStatistics(Canvas canvas) {
        
        drawWord(canvas, 0, 40, "Minutes Spent Slouching:");
        
        int y = 100; //random starting location
        String message;
        //TODO draw all slouch statistics
        for (int i = 0; i < numDays; i++) {
            
            message = dayToString(days[(today - i) % 7]);
            message = message + ": " + slouchMinutes[i] + " minutes";
            drawWord(canvas, 0, y, message);
            y += 60; //move the next message down a little lower
        }
        
    }
    
    /**
     * Draws the word on the screen, this scales in accordance to the number of days
     * @param canvas - to draw one
     * @param x - x coordinate
     * @param y - y coordinate
     * @param word - word to write
     */
    void drawWord(Canvas canvas, int x, int y, String word) {
        
        Paint p = new Paint();
        p.setColor(Color.BLACK);
        p.setTextAlign(Paint.Align.LEFT);
        p.setTypeface(Typeface.SANS_SERIF);
        p.setTextSize(30);
        canvas.drawText(word, x, y, p);
        
    }
    
    /**
     * Overrides the onDraw command, based on flag it draws the appropriate object(s)
     */
    @Override
    protected void onDraw(Canvas canvas) {
        
        if (showStatistics) {
            
            drawStatistics(canvas);

        }
        
        if (showGraph) {
            
            drawArrays(canvas);
            drawDays(canvas);
            drawLegend(canvas);
            
        }
        
    }

    
    /**
     * Helper Method to populate Arrays
     */
    private void populateArrays() {
        
        usageRects = new ArrayList<ShapeDrawable>();
        createRects(usageRects, usageMinutes, 0); 
        productiveRects = new ArrayList<ShapeDrawable>();
        createRects(productiveRects, productiveMinutes, 1);
        slouchRects = new ArrayList<ShapeDrawable>();
        createRects(slouchRects, slouchMinutes, 2); 
                
    }

    /**
     * Helper method to set Dimensions
     */
    private void setDimensions() {
        
        numDays = usageMinutes.length;
        dayWidth = (viewWidth / numDays) - BUFFER; 
        barWidth = dayWidth / 3; //showing three statistics
        
    }
    
    /**
     * Helper method to set colors
     */
    private void setColors() {
        
        colors = new int[3];
        colors[0] = Color.BLUE;
        colors[1] = Color.RED;
        colors[2] = Color.GREEN;
        
    }
    
    /**
     * Sets statistics and draws the graph
     * @param usageMinutes - total minutes
     * @param productiveMinutes - productive minutes
     * @param slouchMinutes - slouch minutes
     * @param today - Day in which to begin chart on
     */
    public void setStatistics(int[] usageMinutes, 
            int[] productiveMinutes, 
            int[] slouchMinutes,
            int today) {
        
        this.usageMinutes = usageMinutes;
        this.productiveMinutes = productiveMinutes;
        this.slouchMinutes = slouchMinutes;
        this.today = today;
        
        setMinuteHeight();
        setUpGraph();
        
        showStatistics = true;
    }
    
    private void setUpGraph() {
        
        //sets colors for each bar
        setColors();
        
        //calculate the dimensions of each bar
        setDimensions();
        
        //fills statistics ArrayLists with rectangles of appropriate sizes
        populateArrays();

        //force redraw
        invalidate();
        
    }
    
    
    
}