package com.example.trackme;

import android.os.AsyncTask;
import android.widget.Button;

/**
 * TimeoutTask Class
 *  ~ TimeoutTask - Launches the timeout request to the server
 * @authors Alex Miller, Connor Liddic
 */
public class TimeoutTask extends AsyncTask<String, Void, String>{
    Button button = null;

    /**
     * Override doInBackground
     * @param - arg0: String message to send 
     */
    @Override
    protected String doInBackground(String... arg0) {
        
        Client client = Client.getClient();
        String message = arg0[0];
        String answer = client.readWrite(message);
        if (message.equals("timeout5")) {//if message is for 5 seconds, wait 5 seconds here
            goToSleep(5000);
        }
        else if (message.equals("timeout7")) {//if message is for 7 seconds, wait 7 seconds here
            goToSleep(7000);
        }
        else if (message.equals("timeout9")) {//if message is for 9 seconds, wait 9 seconds here
            goToSleep(9000);
        }
        return answer;

    }
    
    @Override
    protected void onPostExecute(String string) {
        if (button != null) {//once the thread has arrived here, enable the button as enough time has elapsed for the user to continue
            button.setEnabled(true);//NOTE: The arduino IS asleep! This was simpler to just have the Android go to sleep as well, and then send a confirmation signal to confirm it's good to go
        }
    }
    
    /**
     * Wrapper function for Thread.sleep
     * @param sleep - Miliseconds to sleep for
     */
    public void goToSleep(int sleep) {
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Setter for thebutton
     * @param b - Button to set
     */
    public void setButton(Button b) {
        button = b;
    }

}
