package com.example.trackme;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;

/**
 * Super Activity
 *  ~ Super Class of all Activities, extends Activity. 
 *  ~ Allows all subclasses to call the onCreateDialog
 * @author Alex Miller & Connor Liddic
 */
public class SuperActivity extends Activity{

    /**
     * Override onCreate()
     */
    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 1) {
            return makeHourlyPrompt();    
        }
        else if (id == 2){
            return makeErrorPrompt();
        }
        else {
            return makeWarningPrompt();
        }
    }
    
    @Override
    public void onDestroy() {
        
        super.onDestroy();
        System.out.println("????????????????");
        //finish
        Intent i = new Intent();
        setResult(RESULT_OK, i);
        
        //pop off stack
        if (! MainActivity.activityStack.isEmpty()) {
            MainActivity.activityStack.pop();
            MainActivity.currentActivity = MainActivity.activityStack.peek();
        }

    }
    
    
    /**
     * Makes the error prompt when there was an error and the server was shut down
     * @return Dialog that contains the prompt
     */
    private Dialog makeErrorPrompt() {
            
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            
            //set the message
            String message = "             *** SORRY ***     \n\nThere's been an error with the server and we must close out this session. You should know that you've spent "+ MainActivity.time + " during this session. Sorry again."; 
            builder.setMessage(message);
            
            //set negative button
            builder.setNegativeButton("I promise to not unplug the server again.",
                    new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int id) {
                               
                               //reset all beginning flags
                               MainActivity.firstTimeRunning = true;
                               MainActivity.appIsRunning = false;
                               SecretService.serviceIsActive = false;
                               MainActivity.currentActivity.moveTaskToBack(true);
                               
                               //clear stack
                               MainActivity.activityStack.clear();
                               
                               //close dialog and finish()
                               dialog.cancel();
                               finish();
                           }
                    });
            return builder.create();
            
    }
    
    /**
     * Makes the Warning Prompt - when user spent too much time at the computer
     * @return Dailog to display
     */
    private Dialog makeWarningPrompt() {
    
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        
        //set the string to builder
        String message = "             *** HEY ***     \n\nYou've passed the time that you alloted for yourself. \nYou shouldn't use your computer. \nWe can't stop you, but please go away. \nPleas?."; 
        builder.setMessage(message);
        
        //set button
        builder.setNegativeButton("Thanks for the notification!",
                new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                           //only need to cancel the dialog
                           dialog.cancel();
                       }
                });
        return builder.create();
    
}    

    /**
     * Make the hourly prompt to log stats
     * @return Dialog to display
     */
    private Dialog makeHourlyPrompt() {
        
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        
        //set message to builder
        String message = "   *** UPDATE ***\n You've been at the computer for "+ MainActivity.time + " during this session.\n Have you been productive? Or are you wasting time on tumblr?"; 
        builder.setMessage(message);
        
        //set button
        builder.setNegativeButton("No... I'll be better!",
                new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                           new TrackTask().execute("productivity_n");//send message that they were NOT productive
                           dialog.cancel();
                       }
                     });
        builder.setPositiveButton("Yes! I was productive!",
            new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       new TrackTask().execute("productivity_y");//send message that they were productive
                       dialog.cancel();
                   }
                 });
        return builder.create();
        
    }
    
}
