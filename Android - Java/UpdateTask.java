package com.example.trackme;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.TextView;

/**
 * Update Task Class
 *  ~ Constantly checks every minute to display the current time
 * @authors Alex Miller, Connor Liddic
 */
public class UpdateTask extends AsyncTask<String, Void, String>{

    public static int sleepLength = 5000;
    TextView minuteView = null;
    MainActivity activity;
    
    @Override
    protected String doInBackground(String... params) {

        try {
            Thread.sleep(sleepLength);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Client client = Client.getClient();
        String message = params[0];
        
        String answer = client.readWrite(message);

        return answer;
        
    }
    
    @Override
    protected void onPostExecute(String string) {
        if (string == null) {//if null, then error
            Activity currentActivity = MainActivity.currentActivity;// get current Activity
            currentActivity.removeDialog(2);//show error dialog in currentActivity
            currentActivity.showDialog(2);
        }
        else {//if not null, then update time normally
            String time = getTime(string);
            MainActivity.time = time;
            minuteView.setText(time);
            
            if (MainActivity.appIsRunning) {//if the app is running, then spawn another thread to check again in a while
                UpdateTask t = new UpdateTask();
                t.setView(minuteView, activity);
                t.execute("send_session_len");
            }
        }
        
    }
    
    //----------------------------------------------------------------------------HELPERS
    
    /**
     * Takes the string and returns a time in a nice format
     * @param string - takes a string of minutes
     * @return Clean formatted string of time: "2 hour, 4 minutes"
     */
    private String getTime(String string) {
        String minuteReading = string.substring(4);
        int totalMinutes = Integer.parseInt(minuteReading);
        int hours = totalMinutes / 60;
        int minutes = totalMinutes % 60;
        String returnString = "";
        if (hours == 0) {
            returnString = addMinutes(minutes) + "";
        }
        else if (hours == 1) {
            returnString = "1 hour, " + addMinutes(minutes);
        }
        else {
            returnString = hours + " hours, " + addMinutes(minutes);
        }
        
        return returnString;
    }

    /**
     * Helper to get the correct number of 's's on the string
     * @param minutes the number of minutes
     * @return String of minutes
     */
    private String addMinutes(int minutes) {
        if (minutes == 0) {
            return "0 minutes";
        }
        else if (minutes == 1) {
            return "1 minute";
        }
        else {
            return minutes + " minutes";
        }
    }
    
    //----------------------------------------------------------------------------GETTERS AND SETTERS

    /**
     * Setter to do the view and activity
     * @param minView - view to set
     * @param a - MainActivity to set
     */
    public void setView(TextView minView, MainActivity a) {
        activity = a;
        minuteView = minView;
    }
}
