package com.example.trackme;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * StreamActivity Class
 *  ~ Home Screen, launches all other Activities
 * @authors Alex Miller, Connor Liddic
 */
public class StreamActivity extends SuperActivity {
    
    public static boolean streamMode = true;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        
        //call constructors
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stream_activity);
        
        //get the views
        TextView pressureView = (TextView)findViewById(R.id.armScore);
        TextView proxView = (TextView)findViewById(R.id.backScore);

        //set views to the original message
        pressureView.setText("Warming up...");
        proxView.setText("Warming up...");
        
        //set flag, launch the AsynchTask
        streamMode = true;
        StreamTask t = new StreamTask();
        t.setViews(pressureView, proxView);
        t.execute("stream");
        
        //push on stack
        MainActivity.activityStack.push(this);
        MainActivity.currentActivity = this;
        
    }

    /**
     * Handler for the quit button
     * @param view - that called it
     */
    public void onQuitStreamingClick(View view) {
        
        //set this mode to false, this flag cancels the recurring thread
        streamMode = false;
        
        //pop off thread
//        MainActivity.activityStack.pop();
//        MainActivity.currentActivity = MainActivity.activityStack.peek();
//        
//        //finish
//        Intent i = new Intent();
//        setResult(RESULT_OK, i);
        finish();
    }
    
}
