package com.example.trackme;

import android.app.Activity;
import android.os.AsyncTask;

/**
 * StatusTask Class
 *  ~ AsynchTask that waits for a set amount of time, then pops up the notification
 * @authors Alex Miller, Connor Liddic
 */
public class StatusTask extends AsyncTask<String, Void, String>{

    public static int sleepLength = 100000;//set to '10 minutes', this should really be an hour

    
    @Override
    protected String doInBackground(String... params) {
        
        //go to sleep
        try {
            Thread.sleep(sleepLength);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;

    }
    
    

    @Override
    protected void onPostExecute(String string) {
        if (MainActivity.appIsRunning) {
            
            //get thr current activity, then show the prompt
            Activity currentActivity = MainActivity.currentActivity;
            currentActivity.removeDialog(1);
            currentActivity.showDialog(1);
            
            //as long as the app is running, launch another task
            if (MainActivity.appIsRunning) {
                new StatusTask().execute();
            }
        }
    }
}
