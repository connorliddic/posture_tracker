package com.example.trackme;

import com.example.trackme.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * StatActivity Class
 *  ~ Holds the Stat Section
 * @authors Alex Miller, Connor Liddic
 */
public class StatActivity extends SuperActivity{

    
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
        //calls constructors and sets views
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stat_activity);
        
        //launch Asynchtask that fetches this information
        new StatTask().execute("send_usage");
        
        //pushes onto stack
        MainActivity.activityStack.push(this);
        MainActivity.currentActivity = this;

        //sets the height
        GraphView.viewHeight = getApplicationContext().getResources().getDisplayMetrics().heightPixels - 150; 
        GraphView.viewWidth = getApplicationContext().getResources().getDisplayMetrics().widthPixels;

    }
    
    /**
     * Toggles the Graph
     * @param view - that called it
     */
	public void onToggleGraph(View view) {
			
		if (GraphView.showGraph) {
			
			GraphView.showGraph = false;
			GraphView.showStatistics = true;
		}
		else {
			
			GraphView.showStatistics = false;
			GraphView.showGraph = true;
			
		}
		GraphView.currentGraphView.invalidate();
	}
    
	 /**
     * Handler for the get back click
     * @param view - that called it
     */
	public void onGetBack(View view) {
	
        //disable the views
		GraphView.showGraph = false;
		GraphView.showStatistics = false;
		finish();
	}
	
	/**
	 * Handler for the quit click
	 * @param view - that called it
	 */
    public void onQuitClick(View view) {

        finish();
    }
    
    
    
    
}