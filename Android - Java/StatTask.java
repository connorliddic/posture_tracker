package com.example.trackme;

import java.util.StringTokenizer;
import android.app.Activity;
import android.os.AsyncTask;

/**
 * Stat Task Class
 *  ~ Home Screen, launches all other Activities
 * @authors Alex Miller, Connor Liddic
 */
public class StatTask extends AsyncTask<String, Void, String>{
    
    @Override
    protected String doInBackground(String... params) {
        
        Client client = Client.getClient();
        String message = params[0];
        
        String answer = client.readWrite(message);
        return answer;

    }
    
    

    @Override
    protected void onPostExecute(String string) {
        
        if (string == null) {//is string null, then there was an error in writing to server
            Activity currentActivity = MainActivity.currentActivity;//get the current Activity
            currentActivity.removeDialog(2);//show the error dialog from SuperActivity using the currentActivity
            currentActivity.showDialog(2);
        }
        else {//if received normally, continue here
            parseUsageStatistics(string);//update stats
            
            GraphView.currentGraphView.setStatistics(MainActivity.usageMinutes, //redraw graph
                    MainActivity.productiveMinutes, 
                    MainActivity.slouchMinutes, 
                    Days.FRIDAY);
        }
    }

    /**
     * Counts the days
     * @param string - String to parse
     * @return int number of days
     */
    public static int countDays(String string) {
        
        int numDays = 0;
        int strLen = string.length();
        
        for (int i = 0; i < strLen; i++) {
            if (string.charAt(i) == '#')   numDays++;
        }
        
        return numDays;
        
    }
    
    /**
     * Parses the usage stats that were received from the Server
     * Populates the static fields in the MainActivity class
     * @param string - String to parse, in format #XXX.XXX.XXX
     */
    public static void parseUsageStatistics(String string) {
        
        int numDays = countDays(string);
        
        MainActivity.usageMinutes = new int[numDays];
        MainActivity.productiveMinutes = new int[numDays];
        MainActivity.slouchMinutes = new int[numDays];

        StringTokenizer strToker = new StringTokenizer(string, "#.");
        
        for (int i = 0; i < numDays; i++) {
            
            MainActivity.usageMinutes[i] = Integer.parseInt(strToker.nextToken());
            MainActivity.productiveMinutes[i] = Integer.parseInt(strToker.nextToken());
            MainActivity.slouchMinutes[i] = Integer.parseInt(strToker.nextToken());
            
        }

    }
    
    
}
