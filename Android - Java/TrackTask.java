package com.example.trackme;

import android.os.AsyncTask;
import android.widget.TextView;

/**
 * Track Task Class
 *  ~ Basic AsynchTask class, handles simple connections to server and updates a view with the string
 * @authors Alex Miller, Connor Liddic
 */
public class TrackTask extends AsyncTask<String, Void, String>{
    TextView view = null;

    @Override
    protected String doInBackground(String... arg0) {
        
        Client client = Client.getClient();

        String answer = client.readWrite(arg0[0]);
        return answer;

    }
    
    @Override
    protected void onPostExecute(String string) {
        if (view != null) {//if a view was supplied, then update it with the string received
            view.setText(string);
        }

    }
    
    /**
     * Setter for the private view
     * @param v - TextView to set
     */
    public void setView(TextView v) {
        view = v;
    }

}
