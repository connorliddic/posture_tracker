package com.example.trackme;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.TextView;

/**
 * StreamTask Class
 *  ~ Manages the Stream window
 * @authors Alex Miller, Connor Liddic
 */
public class StreamTask extends AsyncTask<String, Void, String>{

    TextView pressureView = null;
    TextView proxView = null;
    public static int sleepLength = 5000;
    
    
    @Override
    protected String doInBackground(String... params) {
        
        try {
            Thread.sleep(sleepLength);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Client client = Client.getClient();
        String message = params[0];
        
        String answer = client.readWrite(message);
        return answer;
        
    }
    
    

    @Override
    protected void onPostExecute(String string) {
        if (string == null) {//if null, then error and break out by showing error dialog
            Activity currentActivity = MainActivity.currentActivity;
            currentActivity.removeDialog(2);
            currentActivity.showDialog(2);
        }
        else {//else continue
            
            //get readings from string and set to views
            String proxReading = string.substring(4, 7) + " / 1.0";
            String pressureReading = string.substring(11)  + " / 1.0";
            pressureView.setText(pressureReading);
            proxView.setText(proxReading);
            
            //if still in stream mode, launch a new AsynchTask
            if (StreamActivity.streamMode) {
                StreamTask t = new StreamTask();
                t.setViews(pressureView, proxView);
                t.execute("stream");
            }
            
            //if not in this mode, then we may need to send one last request closing this stream
            else {
                if (string.equals("stream_ended")) {//if we received this, then the server isn't listening for this streaming data
                    this.cancel(true);
                }
                else {
                    StreamTask t = new StreamTask();//send one last message to server to close the stream
                    t.setViews(pressureView, proxView);
                    t.execute("end_stream");
                    this.cancel(true);
                }
            }
        }
    }
    
    /**
     * Setter for the views
     * @param preView - pressure view to set
     * @param proView - prox view to set
     */
    public void setViews(TextView preView, TextView proView) {
        pressureView = preView;
        proxView = proView;
    }

}
