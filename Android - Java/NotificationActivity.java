package com.example.trackme;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Notification Activity Class
 *  ~ Activity to set how often the user wants to be bothered to get off the computer
 * @authors Alex Miller, Connor Liddic
 */
public class NotificationActivity extends SuperActivity{
    
    public static int timeWarning = 500;
    
    /**
     * Overrides the onCreate method
     * Sets up the view, calls super-constructor, pushes this activity onto the stack
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        MainActivity.activityStack.push(this);
        MainActivity.currentActivity = this;
    }
    
    /**
     * Handler for the two hour click, sets limit to 5
     * @param view - view called from
     */
    public void onThirtyClick(View view) {
        
        timeWarning = 5;
        closeActivity();

    }
    
    /**
     * Handler for the two hour click, sets limit to 10
     * @param view - view called from
     */
    public void onOneHourClick(View view) {
        
        timeWarning = 10;
        closeActivity();

    }
    
    /**
     * Handler for the two hour click, sets limit to 20
     * @param view - view called from
     */
    public void onTwoHourClick(View view) {
        
        timeWarning = 20;
        closeActivity();

    }
    
    private void closeActivity() {
        finish();
    }
}
