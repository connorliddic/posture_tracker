package com.example.trackme;

import android.app.Activity;
import android.os.AsyncTask;

/**
 * Notification Task Class
 *  ~ Launches the Notification if they have gone over their self imposed time limit
 * @authors Alex Miller, Connor Liddic
 */
public class NotificationTask extends AsyncTask<String, Void, String>{

    static public int sleepLength = 10000;
    
    @Override
    protected String doInBackground(String... params) {
        
        //wait first
        try {
            Thread.sleep(sleepLength);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        //write message to client
        Client client = Client.getClient();
        String response = client.readWrite("send_usage");
        return response;
    }
    
    

    @Override
    protected void onPostExecute(String string) {
        if (MainActivity.appIsRunning) {
            if (string == null) {//if null, do the usual error check
                Activity currentActivity = MainActivity.currentActivity;
                currentActivity.removeDialog(2);
                currentActivity.showDialog(2);
            }
            else {//otherwise, continue
                
                //update the stats in MainActivity
                StatTask.parseUsageStatistics(string);
                
                //access the data in MainActivity and set it to Time
                int time = MainActivity.usageMinutes[0];
                
                //if the total time is larger than the threshold set, then show the warning dialog
                if (time >= NotificationActivity.timeWarning) {
                    Activity currentActivity = MainActivity.currentActivity;
                    currentActivity.removeDialog(3);
                    currentActivity.showDialog(3);
                }
                else {//if it's not time to notify, then launch a new thread to check again
                    new NotificationTask().execute();
                }
            }
        }
    }
}
