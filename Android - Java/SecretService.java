package com.example.trackme;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * SecretService Class
 *  ~ Secret Service, holds the offline notification alert
 * @authors Alex Miller, Connor Liddic
 */
public class SecretService extends IntentService {

    private static final int NOTIFICATION_ID = 1;
    public static int sleepLength = 5000;
    static public boolean serviceIsActive = true;
    
    public SecretService(String name) {
        super(name);
    }
    
    public SecretService() {
        super("SecretService!");
    }
    
    
    /**
     * Override the onHandleIntent
     * Service will now launch from here
     */
    @Override
    protected void onHandleIntent(Intent arg0) {
        
        //loop to continue doing until it is reset from another class - ie there is a system crash
        while(serviceIsActive) {
            
            //if the app is off, check every 5 seconds to see if we can alert the user that a new session began
            if (! MainActivity.appIsRunning) {
                
                //sleep 5 seconds, ask if it's time to notify user
                threadSleep(sleepLength);
                Client client = Client.getClient();
                String answer = client.readWrite("at_computer");
                
                //if null, there was a purposeful crash and we should stop checking(system needs to be reset)
                if (answer == null) {
                    serviceIsActive = false;
                }
                else if (answer.equals("yes")) {//if able to notify, then notify
                    showNotification();    
                }
            }
            //if app on, then go to sleep more often and check occasionally to see the app is off
            else {
                threadSleep(sleepLength * 4);
            }
        }
    }

    /**
     * Wrapper for sleeping
     * @param miliseconds - miliseconds to sleep
     */
    private void threadSleep(int miliseconds) {
        try {
            Thread.sleep(miliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Notification to show
     */
    public void showNotification(){
        
        String ns = Context.NOTIFICATION_SERVICE;
        
        NotificationManager manager = (NotificationManager) getSystemService(ns);
        
        int icon = R.drawable.ic_action_search;
        
        CharSequence tickerText = "TrackMe beginning...";
        
        long when = System.currentTimeMillis();
        
        Notification notification = new Notification(icon, tickerText, when);
        Context context = getApplicationContext();
        
        CharSequence contentTitle = "TrackMe sensed motion and began.";
        CharSequence contentText = "Log on to see your stats!";
        
        Intent notificationIntent = new Intent(this, this.getClass());        
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        
        notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
        
        manager.notify(NOTIFICATION_ID, notification);
        
        
    }
    
}
