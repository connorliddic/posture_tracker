package com.example.trackme;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Timeout Activity Class
 *  ~ Activity to Call timeout in
 * @authors Alex Miller, Connor Liddic
 */
public class TimeoutActivity extends SuperActivity {

    Button rdyToGoButton;
    
    /**
     * Overrides onCreate()
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeout_activity);
        
        //disable to go button
        rdyToGoButton = (Button)findViewById(R.id.readyToGo);
        rdyToGoButton.setEnabled(false);
        
        //add this activity to the stack
        MainActivity.activityStack.push(this);
        MainActivity.currentActivity = this;
    }
    
    /**
     * Toggles all buttons
     */
    public void toggleButtons() {
        Button fiveMinButton = (Button)findViewById(R.id.fiveMin);
        fiveMinButton.setEnabled(false);
        fiveMinButton.invalidate();

        Button sevenMinButton = (Button)findViewById(R.id.sevenMin);
        sevenMinButton.setEnabled(false);

        Button nineMinButton = (Button)findViewById(R.id.nineMin);
        nineMinButton.setEnabled(false);
        
        Button quitButton = (Button)findViewById(R.id.quitButton);
        quitButton.setEnabled(false);
        
        TextView messageText = (TextView)findViewById(R.id.messageView);
        messageText.setText("Go for a walk, or just get off your computer. Shoo.");
        
        //calls invalidate to redraw all buttons
        fiveMinButton.invalidate();
        messageText.invalidate();
        sevenMinButton.invalidate();
        nineMinButton.invalidate();
        quitButton.invalidate();
        
    }
    
    /**
     * Handler for the rock and roll button
     * Closes this activity 
     * @param view - called from
     */
    public void onWakeupClick(View view) {
        new TimeoutTask().execute("wake_up");
//        Intent i = new Intent();
//        setResult(RESULT_OK, i);
//        
//        MainActivity.activityStack.pop();
//        MainActivity.currentActivity = MainActivity.activityStack.peek();
        
        finish();
    }
    
    /**
     * Handler for fiveMinClick
     * Sends signal to C, which is sent to Arduino to power off for 5 minutes
     * @param view - called from
     */
    public void onFiveMinClick(View view) {
        toggleButtons();
        TimeoutTask t = new TimeoutTask();
        t.setButton(rdyToGoButton);
        t.execute("timeout5");
    }
    
    /**
     * Handler for sevenMinClick
     * Sends signal to C, which is sent to Arduino to power off for 7 minutes
     * @param view - called from
     */
    public void onSevenMinClick(View view) {
        toggleButtons();
        TimeoutTask t = new TimeoutTask();
        t.setButton(rdyToGoButton);
        t.execute("timeout7");
    }
    
    /**
     * Handler for nineMinClick
     * Sends signal to C, which is sent to Arduino to power off for 9 minutes
     * @param view - called from
     */
    public void onNineMinClick(View view) {
        toggleButtons();
        TimeoutTask t = new TimeoutTask();
        t.setButton(rdyToGoButton);
        t.execute("timeout9");
    }
    
    /**
     * Handler for quit button
     * closes activity
     * @param view - called from
     */
    public void onQuitClick(View view) {
//        Intent i = new Intent();
//        setResult(RESULT_OK, i);
//        
//        MainActivity.activityStack.pop();
//        MainActivity.currentActivity = MainActivity.activityStack.peek();
        
        finish();
    }
 
    
}
