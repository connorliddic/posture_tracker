package com.example.trackme;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Client Class
 *  ~ Handles all connections communication with Server
 * @authors Alex Miller, Connor Liddic
 */
public class Client {
    final static int PORT_NUM = 12345;
    final static String hostName = "10.0.2.2";
    public static Socket echoSocket = null;
    public static PrintWriter out = null;
    public static BufferedReader in = null;
    public static final Client client = new Client();

    /**
     * Getter for the singleton Client class
     * @return Client singleton class
     */
    public static Client getClient() {
        return client;
    }
    
    public Client() {
                
    }
    
    /**
     * Writes a message to the socket, and returns a string that was the response
     * This is a synchronized method
     * @param message - message to write
     * @return string - message received
     */
    public synchronized String readWrite(String message){
        
        out.println(message);
                
        try {
            return in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return null;
    }

    /**
     * Sets up connection to the Server
     * @param portNum - Port Number to open
     * @throws UnknownHostException
     * @throws IOException
     */
    public static void setUpConnection(int portNum) throws UnknownHostException, IOException {
        
       echoSocket = new Socket(hostName, portNum);
       out = new PrintWriter(echoSocket.getOutputStream(), true);
       in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
        
    }
    
    /**
     * Closes all connections
     */
    public static void closeAll(){
         
        out.close();
        try {
            in.close();
            echoSocket.close();
        } catch (IOException e) {
            System.out.println("Error closing.");
            e.printStackTrace();
        }
        
    }
    
}